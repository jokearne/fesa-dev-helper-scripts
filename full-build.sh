#!/bin/bash

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

source "$SCRIPT_DIR"/env

function usage () {
    echo "full-build.sh [--sync] Deploy_Unit"
}

if [[ $1 == "-h" ]] || [[ $# -eq 0 ]]; then
    usage
    exit 1
fi

if [[ $# -eq 1 ]] && [[ ! $1 == *_DU ]]; then
    usage
    exit 1
else
    DU=$1
fi

DO_SYNC=false
if [[ $# -eq 2 ]] && [[ $1 == --sync ]]; then
    DO_SYNC=true
    DU=$2
fi

set_paths $DU

if [[ $DO_SYNC == true ]]; then
    echo  "$FESA3 --sync $CLASS/src/$CLASS_BASE.design"
    $FESA3 --sync $CLASS/src/$CLASS_BASE.design

    echo "$FESA3 --sync $DU/src/$DU_BASE.deploy"
    $FESA3 --sync $DU/src/$DU_BASE.deploy
fi

cd $CLASS && $FESA_MAKE

cd $DU && $FESA_MAKE
