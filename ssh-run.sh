#!/bin/bash

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

source "$SCRIPT_DIR"/env

function usage () {
  echo "usage: ssh-run.sh [ Class DU ] [ FEC ]"
}

if [[ $# -eq 0 ]] || [[ $1 -eq --no-run ]]; then
    exit 0
fi

if [[ $# -ne 2 ]]; then
  usage
  exit 1
fi

set_paths $1

BIN="$DU/build/bin/L867/*_DU_M"
INSTANCE=/dsc/local/data/BXBPF_DU.**.instance

ssh $2 "cd /dsc/local/data; $BIN -instance $INSTANCE"
